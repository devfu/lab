$LOAD_PATH.unshift File.expand_path '../../lib', __FILE__
require 'lab'

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  Kernel.srand config.seed

  config.filter_run :focus

  config.example_status_persistence_file_path = 'tmp/rspec.txt'
  config.order                                = :random
  config.run_all_when_everything_filtered     = true

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax                                               = :should
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax                 = :should
    mocks.verify_partial_doubles = true
  end
end
