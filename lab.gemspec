# coding: utf-8
lib = File.expand_path '../lib', __FILE__
$LOAD_PATH.unshift lib unless $LOAD_PATH.include? lib
require 'lab/version'

Gem::Specification.new do |spec|
  spec.name          = 'lab'
  spec.version       = Lab::VERSION
  spec.authors       = ['Dev Fu!', 'BM5k']
  spec.email         = %w[ info@devfu.com me@bm5k.com ]

  spec.summary       = 'CLI wrapper for gitlab API'
  spec.description   = 'Like hub, but for gitlab. In ruby.'
  spec.homepage      = 'https://gitlab.devfu.com/devfu/lab'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match %r{^(test|spec|features)/} }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename f }
  spec.require_paths = %w[ lib ]

  spec.required_ruby_version = '~> 2.3.1'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'pry-doc'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'yoshiki'
end
