Contributing
============

In general:

1. Fork
2. Test
3. Code
4. Push
5. Merge request
6. Repeat
