lab
===

Will eventually [hub](hub) but for gitlab. Or similar. Eventually.

Road map:

1. specify api url
2. find repo from current directory's git config
3. list open issues for current repo
4. open merge request for current repo

Other features implemented as needed / wanted

[hub]: https://github.com/github/hub/tree/v1.12.4
